# Install pandas first, "pip install pandas".
import pandas as pd
import numpy as np
from io import StringIO
import re
import sys
import datetime

def get_date():
    isDate = False
    value = ''
    date = ''

    while isDate == False:
        value = input('Insert Date in format dd/mm/yyyy h:m:s -> ')

        try:
            date = datetime.datetime.strptime(value, '%d/%m/%Y %H:%M:%S')
            isDate = True

        except ValueError:
            print('Error: Date format invalid. Try again or press Ctrl + C to exit.')
    return date

def ms_to_rounded_BpM(value):
    return round(60 * 1000 / value)

def get_data(filename):
    # Open the csv containing our data.
    for_pd = StringIO()
    try:
        with open(filename) as records:
            for line in records:
                # As we can have multiple tags separated by commas (the csv separator) we need to swap the first 3 commas of each row to a semi-column.
                new_line = re.sub(r',', ';', line.rstrip(), count=3)
                print (new_line, file=for_pd)

        for_pd.seek(0)

        # Create a dataframe from the adapted csv.
        return pd.read_csv(for_pd, sep=';', names=['type', 'onset', 'offset', 'tags'], header=None)
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
        return None
    except: #handle other exceptions such as attribute errors
        print("Unexpected error:", sys.exc_info()[0])
        return None

def get_premature(df, type):
    return df.loc[(df['type'] == type) & (df['tags'].str.contains('premature'))]

def ms_to_seconds(ms):
    return ms / 1000

def ms_to_days(ms):
    return ms_to_seconds(ms) / (3600 * 24)

def get_heart_rate_summary(qrs):
    # Calculate the interval between each QRS wave.
    qrs = qrs.assign(period=0)
    qrs['period'] = qrs['onset'] - qrs['onset'].shift(1)

    # Calculate the sum of all the periods.
    periods_sum = qrs['period'].sum()

    # Calculate the mean period and the mean heart rate.
    mean_period = periods_sum / len(qrs.index)
    mean_heart_rate = ms_to_rounded_BpM(mean_period)

    # Get the rows containing the min and max onset.
    min_row = qrs.loc[qrs['period'].idxmax()]
    max_row = qrs.loc[qrs['period'].idxmin()]

    # Get the min, max and their datetime.
    min_heart_rate = ms_to_rounded_BpM(min_row['period'])
    max_heart_rate = ms_to_rounded_BpM(max_row['period'])
    min_date = user_date + datetime.timedelta(days=ms_to_days(min_row['onset'].item()),
                                              seconds=ms_to_seconds(min_row['onset'].item()),
                                              milliseconds=min_row['onset'].item())
    max_date = user_date + datetime.timedelta(days=ms_to_days(max_row['onset'].item()),
                                              seconds=ms_to_seconds(max_row['onset'].item()),
                                              milliseconds=max_row['onset'].item())
    print(max_row['onset'].item())

    return mean_period, mean_heart_rate, min_heart_rate, max_heart_rate, min_date, max_date

df = get_data('record.csv')

if type(df) is not bool:
    user_date = get_date()

    # The number of P waves tagged "premature":
    print('P premature : {}'.format(len(get_premature(df, 'P').index)))

    # As well as the number of QRS complexes tagged "premature":
    print('QRS premature : {}'.format(len(get_premature(df, 'QRS').index)))

    # Get a dataframe of all QRS waves
    qrs =  df.loc[df['type'] == 'QRS']

    # Get a summary of the delination
    mean_period, mean_heart_rate, min_heart_rate, max_heart_rate, min_date, max_date = get_heart_rate_summary(qrs)

    # The mean heart rate of the recording (Frequency at which QRS complexes appear):
    print('Mean period QRS: {} ms'.format(mean_period))
    print('Mean heart rate : {} BpM'.format(mean_heart_rate))

    # The minimum and maximum heart rate, each with the time at which they happened.
    # As the date and the time of the recording are not included in the file, the user of the program should be able to set them:
    print('Min heart rate : {0} BpM ({1})'.format(min_heart_rate, min_date.strftime('%d-%m-%Y %H:%M:%S')))
    print('Max heart rate : {0} BpM ({1})'.format(max_heart_rate, max_date.strftime('%d-%m-%Y %H:%M:%S')))


# Bonus question: We want to efficiently host delineations online, and be able to
# quickly download a range of it (e.g. the record between 2 and 3pm on the third
# day). How would you achieve that ?

# I would setup a REST web service (probably with Flask), and make an endpoint taking the desired range in input
# Then it would use that range to create a new dataframe that I would send as a csv file in the response of the request (or a graph generated via matplotlib for example)